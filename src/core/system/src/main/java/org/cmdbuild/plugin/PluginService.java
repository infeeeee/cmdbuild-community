/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package org.cmdbuild.plugin;

/**
 * Placeholder for services used in plugins
 *
 * @author afelice
 */
public interface PluginService {

    public String getName();

    default boolean isDummy() {
        return false;
    }
}
