package org.cmdbuild.config;

import java.time.Duration;
import javax.annotation.Nullable;

/**
 *
 * @author ataboga
 */
public interface MobileConfiguration {

    Boolean isMobileEnabled();

    String getMobileCustomerCode();

    String getMobileDeviceNamePrefix();

    String getMobileNotificationAuthInfo();

    @Nullable
    Duration getArchivedMessageTimeToLive();

}
