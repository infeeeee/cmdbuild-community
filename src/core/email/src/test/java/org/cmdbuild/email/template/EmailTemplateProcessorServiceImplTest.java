/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package org.cmdbuild.email.template;

import java.util.List;
import org.cmdbuild.api.CmApiService;
import org.cmdbuild.auth.role.RoleRepository;
import org.cmdbuild.auth.user.OperationUserSupplier;
import org.cmdbuild.auth.user.UserRepository;
import org.cmdbuild.classe.access.UserClassService;
import org.cmdbuild.common.beans.IdAndDescriptionImpl;
import org.cmdbuild.common.localization.LanguageService;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.beans.CardImpl;
import static org.cmdbuild.dao.constants.SystemAttributes.ATTR_ID;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.core.q3.QueryBuilder;
import org.cmdbuild.dao.core.q3.WhereOperator;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.entrytype.ClassType;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.postgres.q3.RefAttrHelperServiceExt;
import org.cmdbuild.dms.DmsService;
import org.cmdbuild.dms.dao.DmsModelDocument;
import org.cmdbuild.dms.dao.DocumentInfoRepository;
import org.cmdbuild.dms.inner.DocumentInfoAndDetail;
import org.cmdbuild.easytemplate.EasytemplateService;
import org.cmdbuild.easytemplate.FtlTemplateService;
import org.cmdbuild.easytemplate.store.EasytemplateRepository;
import org.cmdbuild.email.EmailAttachment;
import org.cmdbuild.email.EmailSignatureService;
import org.cmdbuild.email.beans.EmailAttachmentImpl;
import static org.cmdbuild.email.utils.EmailMtaUtils.renameDuplicates;
import org.cmdbuild.lookup.LookupService;
import org.cmdbuild.report.ReportService;
import org.cmdbuild.services.permissions.DummyPermissionsHandler;
import org.cmdbuild.services.permissions.PermissionsHandlerProxy;
import org.cmdbuild.services.serialization.attribute.file.CardAttributeFileHelper;
import org.cmdbuild.translation.ObjectTranslationService;
import static org.cmdbuild.utils.lang.CmCollectionUtils.list;
import org.cmdbuild.utils.lang.CmMapUtils;
import static org.cmdbuild.utils.lang.CmMapUtils.map;
import org.hamcrest.Matcher;
import static org.hamcrest.MatcherAssert.assertThat;
import org.hamcrest.Matchers;
import static org.hamcrest.Matchers.endsWithIgnoringCase;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 * @author afelice
 */
public class EmailTemplateProcessorServiceImplTest {

    private final Classe classe = mock(Classe.class);

    private final DaoService dao = mock(DaoService.class);
    private final DmsService dmsService = mock(DmsService.class);
    private final ObjectTranslationService translationService = mock(ObjectTranslationService.class);
    private final DocumentInfoRepository repository = mock(DocumentInfoRepository.class);
    private final UserClassService userClassService = mock(UserClassService.class);
    private final UserRepository userRepository = mock(UserRepository.class);

    private final EasytemplateRepository easytemplateRepository = mock(EasytemplateRepository.class);
    private final EasytemplateService easytemplateService = mock(EasytemplateService.class);
    private final OperationUserSupplier userSupplier = mock(OperationUserSupplier.class);
    private final RoleRepository roleRepository = mock(RoleRepository.class);
    private final FtlTemplateService ftlTemplateService = mock(FtlTemplateService.class);
    private final CmApiService apiService = mock(CmApiService.class);
    private final RefAttrHelperServiceExt refAttrHelperService = mock(RefAttrHelperServiceExt.class);
    private final LookupService lookupService = mock(LookupService.class);
    private final ReportService reportService = mock(ReportService.class);
    private final EmailSignatureService signatureService = mock(EmailSignatureService.class);
    private final LanguageService languageService = mock(LanguageService.class);
    private final DmsAttachmentDownloader dmsAttachmentDownloader = mock(DmsAttachmentDownloader.class);
    private final QueryBuilder queryBuilder = mock(QueryBuilder.class);

    // Skip all permissions check
    PermissionsHandlerProxy permissionsHandler = new DummyPermissionsHandler();

    EmailTemplateProcessorServiceImpl instance;

    @Before
    public void setUp() {
        when(classe.getName()).thenReturn("MyEmail"); // Used in CardAttributeFile_Helper.fillWithCardMetadata()
        when(classe.asClasse()).thenReturn(classe); // Used in CardImpl copy-contructor
        when(classe.getClassType()).thenReturn(ClassType.CT_SIMPLE); // Used in CardImpl contructor

        // used in CardAttributeFileHelper.loadDocument
        when(dao.selectCount()).thenReturn(queryBuilder);
        when(dao.selectCount().from(anyString())).thenReturn(queryBuilder);
        when(dao.selectCount().from(anyString()).includeHistory()).thenReturn(queryBuilder);
        when(dao.selectCount().from(anyString()).where(anyString(), any(WhereOperator.class), anyList())).thenReturn(queryBuilder);

        when(dao.selectCount().from(anyString()).where(anyString(), any(WhereOperator.class), anyList()).getCount()).thenReturn(Long.valueOf(1));
        when(dao.selectCount().from(anyString()).includeHistory().where(anyString(), any(WhereOperator.class), anyList()).getCount()).thenReturn(Long.valueOf(1));

        instance = new EmailTemplateProcessorServiceImpl(easytemplateRepository, easytemplateService, userRepository, userSupplier, roleRepository,
                dao, ftlTemplateService, apiService, refAttrHelperService,
                lookupService, reportService, translationService,
                signatureService, languageService, dmsAttachmentDownloader,
                new CardAttributeFileHelper(dao, dmsService, repository, userClassService, translationService, userRepository,
                        permissionsHandler));
    }

    /**
     * Test of loadClientCardValues method, for File attribute, <b>without a
     * DMS</b>, category active, of class EmailTemplateProcessorServiceImpl.
     */
    @Test
    public void testLoadClientCardValues_FileAttribute_NoDms_CategoryActive() {
        System.out.println("loadClientCardValues_FileAttribute_NoDms_CategoryActive");

        //arrange:
        final String fileAttrName = "FileAttribute";
        final Long cardId = 22L;

        // Attribute
        Attribute fileAttribute = TestDmsHelper.mockBuildAttr_File(fileAttrName, classe, TestDmsHelper.A_KNOWN_CATEGORY_DESCRIPTION);
        when(classe.getAllAttributes()).thenReturn(list(fileAttribute));
        // ...and related card
        Card aRelatedCard = CardImpl.builder()
                .withId(cardId).withType(classe)
                .withAttribute(fileAttrName, TestDmsHelper.A_KNOWN_DOCUMENT_ID)
                .build();

        DmsModelDocument aDocument = TestDmsHelper.mockBuildDmsModelDocument(aRelatedCard);

        // No DMS
        boolean isDmsServiceOk = false;
        TestDmsHelper.mockDmsService(dmsService, isDmsServiceOk);
        // Mock persistence
        QueryBuilder mockQueryBuilder = TestDmsHelper.mockQueryBuilder_SelectAll(dao);
        TestDmsHelper.mockQueryBuilder_where_String(mockQueryBuilder, "DmsModel");
        when(mockQueryBuilder.getCard()).thenReturn(aRelatedCard);
        TestDmsHelper.mockQueryBuilder_whereExpr(mockQueryBuilder, aRelatedCard.getTypeName());
        when(mockQueryBuilder.getCardOrNull()).thenReturn(aRelatedCard);

        when(repository.getById(eq(TestDmsHelper.A_KNOWN_DOCUMENT_ID))).thenReturn(aDocument);
        // Category
        boolean categoryActive = true;
        org.cmdbuild.lookup.LookupValue aCategory = TestDmsHelper.mockCategory(categoryActive);
        when(dmsService.getCategoryLookupForAttachment(any(), any())).thenReturn(aCategory);

        CmMapUtils.FluentMap<String, Object> expResult = map(
                ATTR_ID, cardId,
                // File attribute
                fileAttrName, TestDmsHelper.A_KNOWN_DOCUMENT_ID,
                TestDmsHelper.buildProp(fileAttrName, "name"), TestDmsHelper.A_KNOWN_FILENAME,
                TestDmsHelper.buildProp(fileAttrName, "category"), TestDmsHelper.A_KNOWN_CATEGORY_LOOKUP_ID,
                TestDmsHelper.buildProp(fileAttrName, "Category"), TestDmsHelper.A_KNOWN_CATEGORY_LOOKUP_ID, // for legacy
                TestDmsHelper.buildProp(fileAttrName, "description"), TestDmsHelper.A_KNOWN_DOCUMENT_DESCRIPTION,
                TestDmsHelper.buildProp(fileAttrName, "Description"), TestDmsHelper.A_KNOWN_DOCUMENT_DESCRIPTION, // for legacy
                TestDmsHelper.buildProp(fileAttrName, "version"), TestDmsHelper.A_KNOWN_DOCUMENT_VERSION,
                TestDmsHelper.buildProp(fileAttrName, "author"), TestDmsHelper.A_KNOWN_AUTHOR,
                TestDmsHelper.buildProp(fileAttrName, "created"), TestDmsHelper.A_KNOWN_DOCUMENT_CREATION_DATE_STR,
                TestDmsHelper.buildProp(fileAttrName, "modified"), TestDmsHelper.A_KNOWN_DOCUMENT_MODIFICATION_DATE_STR,
                TestDmsHelper.buildProp(fileAttrName, "isDmsServiceOk"), isDmsServiceOk);
        //act:
        Card cardWithValues = instance.loadClientCardValues(aRelatedCard, null);

        //assert:
        TestDmsHelper.checkEquals_Map(expResult, cardWithValues.getAllValuesAsMap());
    }

    /**
     * Test of loadClientCardValues method, for File attribute, <b>not set
     * documentId</b>, of class EmailTemplateProcessorServiceImpl.
     */
    @Test
    public void testLoadClientCardValues_FileAttribute_Dms_CategoryActive_EmptyCard() {
        System.out.println("loadClientCardValues_FileAttribute_Dms_CategoryActive_EmptyCard");

        //arrange:
        final String fileAttrName = "FileAttribute";
        final Long cardId = 22L;

        // Attribute
        Attribute fileAttribute = TestDmsHelper.mockBuildAttr_File(fileAttrName, classe, TestDmsHelper.A_KNOWN_CATEGORY_DESCRIPTION);
        when(classe.getAllAttributes()).thenReturn(list(fileAttribute));
        // ...and related card
        Card aRelatedCard = CardImpl.builder()
                .withId(cardId).withType(classe)
                // Not set documentId
                //.withAttribute(fileAttrName, TestDmsHelper.A_KNOWN_DOCUMENT_ID)
                .build();

        // DMS
        boolean isDmsServiceOk = true;
        TestDmsHelper.mockDmsService(dmsService, isDmsServiceOk);

        // Category
        boolean categoryActive = true;
        org.cmdbuild.lookup.LookupValue aCategory = TestDmsHelper.mockCategory(categoryActive);

        CmMapUtils.FluentMap<String, Object> expResult = map(
                ATTR_ID, cardId);

        //act:
        Card cardWithValues = instance.loadClientCardValues(aRelatedCard, null);

        //assert:
        verify(dmsService, never()).getCardAttachmentByMetadataId(anyLong());
        verify(dmsService, never()).getCategoryLookupForAttachment(any(), any());
        TestDmsHelper.checkEquals_Map(expResult, cardWithValues.getAllValuesAsMap());
    }

    /**
     * Test of loadClientCardValues method, for File attribute, given documentId
     * (as String), <b>with DMS</b>
     * enabled, category active, of class EmailTemplateProcessorServiceImpl.
     */
    @Test
    public void testLoadClientCardValues_FileAttribute_Dms_CategoryActive_DocumentId() {
        System.out.println("loadClientCardValues_FileAttribute_Dms_CategoryActive_DocumentId");

        //arrange:
        final String fileAttrName = "FileAttribute";
        final Long cardId = 22L;

        // Attribute
        Attribute fileAttribute = TestDmsHelper.mockBuildAttr_File(fileAttrName, classe, TestDmsHelper.A_KNOWN_CATEGORY_DESCRIPTION);
        when(classe.getAllAttributes()).thenReturn(list(fileAttribute));
        // ...and related card
        Card aRelatedCard = CardImpl.builder()
                .withId(cardId).withType(classe)
                .withAttribute(fileAttrName, TestDmsHelper.A_KNOWN_DOCUMENT_ID)
                .build();

        DocumentInfoAndDetail aDocument = TestDmsHelper.mockBuildDocumentInfoAndDetail(aRelatedCard);

        // DMS
        boolean isDmsServiceOk = true;
        TestDmsHelper.mockDmsService(dmsService, isDmsServiceOk);
        when(dmsService.getCardAttachmentById(eq(TestDmsHelper.A_KNOWN_DOCUMENT_ID))).thenReturn(aDocument);

        // Category
        boolean categoryActive = true;
        org.cmdbuild.lookup.LookupValue aCategory = TestDmsHelper.mockCategory(categoryActive);
        when(dmsService.getCategoryLookupForAttachment(any(), any())).thenReturn(aCategory);

        CmMapUtils.FluentMap<String, Object> expResult = map(
                ATTR_ID, cardId,
                // File attribute
                fileAttrName, TestDmsHelper.A_KNOWN_DOCUMENT_ID,
                TestDmsHelper.buildProp(fileAttrName, "name"), TestDmsHelper.A_KNOWN_FILENAME,
                TestDmsHelper.buildProp(fileAttrName, "category"), TestDmsHelper.A_KNOWN_CATEGORY_LOOKUP_ID,
                TestDmsHelper.buildProp(fileAttrName, "Category"), TestDmsHelper.A_KNOWN_CATEGORY_LOOKUP_ID, // for legacy
                TestDmsHelper.buildProp(fileAttrName, "description"), TestDmsHelper.A_KNOWN_DOCUMENT_DESCRIPTION,
                TestDmsHelper.buildProp(fileAttrName, "Description"), TestDmsHelper.A_KNOWN_DOCUMENT_DESCRIPTION, // for legacy
                TestDmsHelper.buildProp(fileAttrName, "version"), TestDmsHelper.A_KNOWN_DOCUMENT_VERSION,
                TestDmsHelper.buildProp(fileAttrName, "author"), TestDmsHelper.A_KNOWN_AUTHOR,
                TestDmsHelper.buildProp(fileAttrName, "created"), TestDmsHelper.A_KNOWN_DOCUMENT_CREATION_DATE_STR,
                TestDmsHelper.buildProp(fileAttrName, "modified"), TestDmsHelper.A_KNOWN_DOCUMENT_MODIFICATION_DATE_STR,
                TestDmsHelper.buildProp(fileAttrName, "isDmsServiceOk"), isDmsServiceOk);

        //act:
        Card cardWithValues = instance.loadClientCardValues(aRelatedCard, null);

        //assert:
        TestDmsHelper.checkEquals_Map(expResult, cardWithValues.getAllValuesAsMap());
    }

    /**
     * Test of loadClientCardValues method, for File attribute, given full
     * generated document cardId (as long), <b>with DMS</b>
     * enabled, category active, of class EmailTemplateProcessorServiceImpl.
     */
    @Test
    public void testLoadClientCardValues_FileAttribute_Dms_CategoryActive_DocumentCardId() throws Exception {
        System.out.println("loadClientCardValues_FileAttribute_Dms_CategoryActive_DocumentCardId");

        //arrange:
        final String fileAttrName = "FileAttribute";
        final Long cardId = 22L;

        // Attribute
        Attribute fileAttribute = TestDmsHelper.mockBuildAttr_File(fileAttrName, classe, TestDmsHelper.A_KNOWN_CATEGORY_DESCRIPTION);
        when(classe.getAllAttributes()).thenReturn(list(fileAttribute));
        // ...and related card
        Card aRelatedCard = CardImpl.builder()
                .withId(cardId).withType(classe)
                .build();

        DocumentInfoAndDetail aDocument = TestDmsHelper.mockBuildDocumentInfoAndDetail(aRelatedCard);

        // In related card put the full (generated) card Id
        aRelatedCard = CardImpl.copyOf(aRelatedCard)
                .withAttribute(fileAttrName, TestDmsHelper.A_KNOWN_DOCUMENT_FULL_GENERATED_CARD_ID)
                .build();

        // DMS
        boolean isDmsServiceOk = true;
        TestDmsHelper.mockDmsService(dmsService, isDmsServiceOk);
        when(dmsService.getCardAttachmentByMetadataId(eq(TestDmsHelper.A_KNOWN_DOCUMENT_FULL_GENERATED_CARD_ID))).thenReturn(aDocument);

        // Category
        boolean categoryActive = true;
        org.cmdbuild.lookup.LookupValue aCategory = TestDmsHelper.mockCategory(categoryActive);
        when(dmsService.getCategoryLookupForAttachment(any(), any())).thenReturn(aCategory);

        CmMapUtils.FluentMap<String, Object> expResult = map(
                ATTR_ID, cardId,
                // File attribute
                fileAttrName, TestDmsHelper.A_KNOWN_DOCUMENT_ID,
                TestDmsHelper.buildProp(fileAttrName, "name"), TestDmsHelper.A_KNOWN_FILENAME,
                TestDmsHelper.buildProp(fileAttrName, "category"), TestDmsHelper.A_KNOWN_CATEGORY_LOOKUP_ID,
                TestDmsHelper.buildProp(fileAttrName, "Category"), TestDmsHelper.A_KNOWN_CATEGORY_LOOKUP_ID, // for legacy
                TestDmsHelper.buildProp(fileAttrName, "description"), TestDmsHelper.A_KNOWN_DOCUMENT_DESCRIPTION,
                TestDmsHelper.buildProp(fileAttrName, "Description"), TestDmsHelper.A_KNOWN_DOCUMENT_DESCRIPTION, // for legacy
                TestDmsHelper.buildProp(fileAttrName, "version"), TestDmsHelper.A_KNOWN_DOCUMENT_VERSION,
                TestDmsHelper.buildProp(fileAttrName, "author"), TestDmsHelper.A_KNOWN_AUTHOR,
                TestDmsHelper.buildProp(fileAttrName, "created"), TestDmsHelper.A_KNOWN_DOCUMENT_CREATION_DATE_STR,
                TestDmsHelper.buildProp(fileAttrName, "modified"), TestDmsHelper.A_KNOWN_DOCUMENT_MODIFICATION_DATE_STR,
                TestDmsHelper.buildProp(fileAttrName, "isDmsServiceOk"), isDmsServiceOk);

        //act:
        Card cardWithValues = instance.loadClientCardValues(aRelatedCard, null);

        //assert:
        TestDmsHelper.checkEquals_Map(expResult, cardWithValues.getAllValuesAsMap());
    }

    /**
     * Test of loadClientCardValues method, for File attribute, given full
     * generated document cardId (as IdAndDescription), <b>with DMS</b>
     * enabled, category active, of class EmailTemplateProcessorServiceImpl.
     */
    @Test
    public void testLoadClientCardValues_FileAttribute_Dms_CategoryActive_DocumentCardIdAndDescription() {
        System.out.println("loadClientCardValues_FileAttribute_Dms_CategoryActive");

        //arrange:
        final String fileAttrName = "FileAttribute";
        final Long cardId = 22L;

        // Attribute
        Attribute fileAttribute = TestDmsHelper.mockBuildAttr_File(fileAttrName, classe, TestDmsHelper.A_KNOWN_CATEGORY_DESCRIPTION);
        when(classe.getAllAttributes()).thenReturn(list(fileAttribute));
        // ...and related card
        Card aRelatedCard = CardImpl.builder()
                .withId(cardId).withType(classe)
                .build();

        DocumentInfoAndDetail aDocument = TestDmsHelper.mockBuildDocumentInfoAndDetail(aRelatedCard);

        // In related card put the full (generated) card Id (as IdAndDescription
        aRelatedCard = CardImpl.copyOf(aRelatedCard)
                .withAttribute(fileAttrName, new IdAndDescriptionImpl(TestDmsHelper.A_KNOWN_DOCUMENT_FULL_GENERATED_CARD_ID, "dummyDescription"))
                .build();

        // DMS
        boolean isDmsServiceOk = true;
        TestDmsHelper.mockDmsService(dmsService, isDmsServiceOk);
        when(dmsService.getCardAttachmentByMetadataId(eq(TestDmsHelper.A_KNOWN_DOCUMENT_FULL_GENERATED_CARD_ID))).thenReturn(aDocument);

        // Category
        boolean categoryActive = true;
        org.cmdbuild.lookup.LookupValue aCategory = TestDmsHelper.mockCategory(categoryActive);
        when(dmsService.getCategoryLookupForAttachment(any(), any())).thenReturn(aCategory);

        CmMapUtils.FluentMap<String, Object> expResult = map(
                ATTR_ID, cardId,
                // File attribute
                fileAttrName, TestDmsHelper.A_KNOWN_DOCUMENT_ID,
                TestDmsHelper.buildProp(fileAttrName, "name"), TestDmsHelper.A_KNOWN_FILENAME,
                TestDmsHelper.buildProp(fileAttrName, "category"), TestDmsHelper.A_KNOWN_CATEGORY_LOOKUP_ID,
                TestDmsHelper.buildProp(fileAttrName, "Category"), TestDmsHelper.A_KNOWN_CATEGORY_LOOKUP_ID, // for legacy
                TestDmsHelper.buildProp(fileAttrName, "description"), TestDmsHelper.A_KNOWN_DOCUMENT_DESCRIPTION,
                TestDmsHelper.buildProp(fileAttrName, "Description"), TestDmsHelper.A_KNOWN_DOCUMENT_DESCRIPTION, // for legacy
                TestDmsHelper.buildProp(fileAttrName, "version"), TestDmsHelper.A_KNOWN_DOCUMENT_VERSION,
                TestDmsHelper.buildProp(fileAttrName, "author"), TestDmsHelper.A_KNOWN_AUTHOR,
                TestDmsHelper.buildProp(fileAttrName, "created"), TestDmsHelper.A_KNOWN_DOCUMENT_CREATION_DATE_STR,
                TestDmsHelper.buildProp(fileAttrName, "modified"), TestDmsHelper.A_KNOWN_DOCUMENT_MODIFICATION_DATE_STR,
                TestDmsHelper.buildProp(fileAttrName, "isDmsServiceOk"), isDmsServiceOk);

        //act:
        Card cardWithValues = instance.loadClientCardValues(aRelatedCard, null);

        //assert:
        TestDmsHelper.checkEquals_Map(expResult, cardWithValues.getAllValuesAsMap());
    }

    /**
     * Test of createEmailFromTemplate method, of class
     * EmailTemplateProcessorServiceImpl.
     */
    @Test
    public void testEmailAttachments_RenameDuplicates() {
        System.out.println("emailAttachments_RenameDuplicates");

        //arrange:
        List<EmailAttachment> emailAttachments = list(mock_buildEmailAttachment_PDF("abc.pdf"),
                mock_buildEmailAttachment_PDF("abc.pdf")
        );

        //act:
        List<EmailAttachment> result = renameDuplicates(emailAttachments);

        //assert:
        assertThat(result, hasSize(2));
        assertFalse(result.get(0).getFileName().equals(result.get(1).getFileName()));
        final Matcher<EmailAttachment> matcherExtension = Matchers.hasProperty("fileName", endsWithIgnoringCase(".pdf"));
        assertThat(result, everyItem(matcherExtension));
    }

    private static EmailAttachmentImpl mock_buildEmailAttachment_PDF(final String fileName) {
        return EmailAttachmentImpl.builder().withFileName(fileName)
                .withData(new byte[0])
                .withContentType("application/pdf")
                .build();
    }

}
