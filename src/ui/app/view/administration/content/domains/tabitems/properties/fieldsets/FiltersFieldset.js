Ext.define('CMDBuildUI.view.administration.content.domains.tabitems.properties.fieldsets.FiltersFieldset', {
    extend: 'Ext.panel.Panel',
    requires: [
        'CMDBuildUI.view.administration.content.domains.tabitems.properties.fieldsets.FiltersFieldsetController',
        'CMDBuildUI.view.administration.content.domains.tabitems.properties.fieldsets.FiltersFieldsetModel'
    ],

    alias: 'widget.administration-content-domains-tabitems-properties-fieldsets-filtersfieldset',

    controller: 'administration-content-domains-tabitems-properties-fieldsets-filtersfieldset',
    viewModel: {
        type: 'administration-content-domains-tabitems-properties-fieldsets-filtersfieldset'
    },
    ui: 'administration-formpagination',
    items: [{
        xtype: 'fieldset',
        layout: 'column',
        title: CMDBuildUI.locales.Locales.administration.groupandpermissions.fieldlabels.filters,
        localized: {
            title: 'CMDBuildUI.locales.Locales.administration.groupandpermissions.fieldlabels.filters'
        },
        itemId: 'domain-filterfieldset',
        ui: 'administration-formpagination',
        items: [CMDBuildUI.util.administration.helper.FieldsHelper.getCommonTextareaInput('inClassReferenceFiltersSource', {
            inClassReferenceFiltersSource: {
                xtype: 'textarea',
                fieldLabel: CMDBuildUI.locales.Locales.administration.domains.fieldlabels.originfilter,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.domains.fieldlabels.originfilter'
                },
                resizable: {
                    handles: "s"
                },
                name: 'inClassReferenceFiltersSource',
                bind: {
                    readOnly: '{actions.view}',
                    value: '{theDomain.inClassReferenceFilters.sourceFilter}'
                },
                listeners: {
                    change: function (textarea, newValue, oldValue) {
                        var vm = textarea.lookupViewModel();
                        vm.get('theDomain.inClassReferenceFilters').sourceFilter = newValue;
                    }
                }
            }
        }), CMDBuildUI.util.administration.helper.FieldsHelper.getCommonTextareaInput('inClassReferenceFiltersDestination', {
            inClassReferenceFiltersDestination: {
                xtype: 'textarea',
                fieldLabel: CMDBuildUI.locales.Locales.administration.domains.fieldlabels.destinationfilter,
                localized: {
                    fieldLabel: 'CMDBuildUI.locales.Locales.administration.domains.fieldlabels.destinationfilter'
                },
                resizable: {
                    handles: "s"
                },
                name: 'inClassReferenceFiltersDestination',
                bind: {
                    readOnly: '{actions.view}',
                    value: '{theDomain.inClassReferenceFilters.destinationFilter}'
                },
                listeners: {
                    change: function (textarea, newValue, oldValue) {
                        var vm = textarea.lookupViewModel();
                        vm.get('theDomain.inClassReferenceFilters').destinationFilter = newValue;
                    }
                }
            }
        })
        ]
    }]
});