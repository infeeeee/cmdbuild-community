/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.cmdbuild.service.rest.common.helpers;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.ImmutableList.toImmutableList;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.cmdbuild.classe.access.UserCardQueryForDomain;
import org.cmdbuild.classe.access.UserCardQueryOptions;
import org.cmdbuild.classe.access.UserCardQueryOptionsImpl;
import org.cmdbuild.classe.access.UserCardService;
import static org.cmdbuild.classe.access.UserCardService.FOR_DOMAIN_HAS_ANY_RELATION;
import static org.cmdbuild.classe.access.UserCardService.FOR_DOMAIN_HAS_THIS_RELATION;
import org.cmdbuild.common.utils.PagedElements;
import org.cmdbuild.dao.beans.Card;
import org.cmdbuild.dao.beans.RelationDirection;
import org.cmdbuild.dao.core.q3.DaoService;
import org.cmdbuild.dao.driver.postgres.q3.DaoQueryOptions;
import org.cmdbuild.dao.entrytype.Attribute;
import org.cmdbuild.dao.entrytype.Domain;
import static org.cmdbuild.dao.entrytype.DomainCardinality.MANY_TO_MANY;
import static org.cmdbuild.dao.entrytype.DomainCardinality.MANY_TO_ONE;
import static org.cmdbuild.dao.entrytype.DomainCardinality.ONE_TO_MANY;
import org.cmdbuild.service.rest.common.serializationhelpers.CardWsSerializationHelperv3;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author afelice
 */
@Component
public class CardsForDomainFetcherImpl implements CardsForDomainFetcher {

    private final DaoService dao;
    private final UserCardService cardService;
    private final CardWsSerializationHelperv3 helper;

    private final Logger logger = LoggerFactory.getLogger(getClass());
//    private final static Predicate isTrue = Predicate.isEqual(true);
    private final static Predicate isFalse = Predicate.isEqual(false);

    public CardsForDomainFetcherImpl(CardWsSerializationHelperv3 helper,
            UserCardService cardService,
            DaoService dao) {
        this.helper = checkNotNull(helper);
        this.cardService = checkNotNull(cardService);
        this.dao = checkNotNull(dao);
    }

    @Override
    public PagedElements<Card> fetchCards(UserCardQueryForDomain forDomain, String classId, DaoQueryOptions queryOptions, String selectFunctionValue) {
        UserCardQueryOptions userQueryOptions = UserCardQueryOptionsImpl.builder().withQueryOptions(queryOptions).withForDomain(forDomain).withFunctionValue(selectFunctionValue).build();
        PagedElements<Card> cards = cardService.getUserCards(classId, userQueryOptions);

        // #7749 remove already related cards:
        // - in autoloop
        // - if searching in N side (ex. User) and current card already related with something in the 1 side (ex. Admin)
        if (forDomain != null) {
            Domain dom = dao.getDomain(forDomain.getDomainName()).getThisDomainWithDirection(forDomain.getDirection());
            if (isAutoLoop(dom)) {
                logger.debug("autoloop detected; orig cards {}", UserCardService.toLog(cards));
                cards = filterOutAlreadyAnyRelated(cards);

                // filter out reversed already related
                cards = filterOutReverseRelated(cards, classId, forDomain.getDirection().inverse(), dom);
            } else if (isManySide(dom, forDomain)) {
                logger.debug("many side in 1:N detected; orig cards {}", UserCardService.toLog(cards));
                if (cardService.isRelatedInDomain(forDomain.getOriginId(), getClassAtManySide(dom), forDomain.getDirection(), dom)) {
                    logger.debug("many side card=< {} > is already related to something; returning none available cards to create a relation (see #7749) ", forDomain.getOriginId());
                    cards = PagedElements.empty();
                }
            } else if (isOneSide(dom, forDomain)) {
                logger.debug("one side in N:1 detected; orig cards {}", UserCardService.toLog(cards));
                if (cardService.isRelatedInDomain(forDomain.getOriginId(), getClassAtOneSide(dom), forDomain.getDirection(), dom)) {
                    logger.debug("one side card=< {} > is already related to something; returning none available cards to create a relation (see #7749) ", forDomain.getOriginId());
                    cards = PagedElements.empty();
                }
            }
            logger.debug("filtered cards {}", UserCardService.toLog(cards));
        }

        return cards;
    }

    @Override
    public List<Map<String, Object>> fetchCardsForDomain(UserCardQueryForDomain forDomain, PagedElements<Card> cards,
            DaoQueryOptions queryOptions, String selectFunctionValue) {
        if (cards.isEmpty()) {
            return emptyList();
        }
        Domain theDom = null;
        if (forDomain != null) {
            theDom = dao.getDomain(forDomain.getDomainName()).getThisDomainWithDirection(forDomain.getDirection());
        }
        final Domain dom = theDom; // now it's final and the following lambda is happy

        return cards.stream().map(c -> helper.serializeCard(c, queryOptions).accept((m) -> {
            if (dom != null) {
                boolean hasAnyRelation = c.get(FOR_DOMAIN_HAS_ANY_RELATION, Boolean.class),
                        hasThisRelation = c.get(FOR_DOMAIN_HAS_THIS_RELATION, Boolean.class),
                        available = (!hasThisRelation || dom.hasDomainKeyAttrs()) && dom.isDomainForTargetClasse(c.getType()) && (!hasAnyRelation || (dom.hasCardinality(MANY_TO_ONE) || dom.hasCardinality(MANY_TO_MANY)));
                m.put(format("_%s_available", dom.getName()), available, format("_%s_hasrelation", dom.getName()), hasThisRelation);
            }
            if (isNotBlank(selectFunctionValue)) {
                Attribute param = dao.getFunctionByName(selectFunctionValue).getOnlyOutputParameter();
                helper.addCardValuesAndDescriptionsAndExtras(param.getName(), param.getType(), c::get, m::put);
            }
        })).collect(toList());
    }

    private static boolean isAutoLoop(Domain dom) {
        return dom.getSourceClass().equals(dom.getTargetClass());
    }

    private static PagedElements<Card> filterOutAlreadyAnyRelated(PagedElements<Card> cards) {
        return buildPagedElements(cards.stream().filter(c -> isFalse.test(c.get(FOR_DOMAIN_HAS_ANY_RELATION, Boolean.class))));
    }

    private static PagedElements<Card> filterOutAlreadyThisRelated(PagedElements<Card> cards) {
        return buildPagedElements(cards.stream().filter(c -> isFalse.test(c.get(FOR_DOMAIN_HAS_THIS_RELATION, Boolean.class))));
    }

    private static PagedElements<Card> buildPagedElements(Stream<Card> cards) {
        return new PagedElements<>(cards.collect(toImmutableList()));
    }

    private boolean isManySide(Domain dom, UserCardQueryForDomain forDomain) {
        return (dom.hasCardinality(ONE_TO_MANY) && forDomain.getDirection().equals(RelationDirection.RD_DIRECT))
                || (dom.hasCardinality(MANY_TO_ONE) && forDomain.getDirection().equals(RelationDirection.RD_INVERSE));
    }

    private boolean isOneSide(Domain dom, UserCardQueryForDomain forDomain) {
        return (dom.hasCardinality(ONE_TO_MANY) && forDomain.getDirection().equals(RelationDirection.RD_INVERSE))
                || (dom.hasCardinality(MANY_TO_ONE) && forDomain.getDirection().equals(RelationDirection.RD_DIRECT));
    }

    private PagedElements<Card> filterOutReverseRelated(PagedElements<Card> cards, String classId, RelationDirection origDirection, Domain domain) {
        logger.debug("filterOutReverseRelated() for many side cards {} on classId=< {} > in domain=< {} > and direction=< {} >(see #7749) ", UserCardService.toLog(cards), classId, domain.getName(), origDirection);
        return buildPagedElements(cards.stream().filter(c -> isFalse.test(
                cardService.isRelatedInDomain(c.getId(), classId, origDirection, domain)
                || cardService.isRelatedInDomain(c.getId(), classId, origDirection.inverse(), domain)
        )));
    }

    private String getClassAtManySide(Domain dom) {
        if (dom.hasCardinality(ONE_TO_MANY)) {
            return dom.getTargetClassName();
        } else if (dom.hasCardinality(MANY_TO_ONE)) {
            return dom.getSourceClassName();
        }

        checkArgument(false, "Expected \"%s\"|\"%s\", found \"%s\"", ONE_TO_MANY, MANY_TO_MANY, dom.getCardinality());
        return null;
    }

    private String getClassAtOneSide(Domain dom) {
        if (dom.hasCardinality(ONE_TO_MANY)) {
            return dom.getSourceClassName();
        } else if (dom.hasCardinality(MANY_TO_ONE)) {
            return dom.getTargetClassName();
        }

        checkArgument(false, "Expected \"%s\"|\"%s\", found \"%s\"", ONE_TO_MANY, MANY_TO_MANY, dom.getCardinality());
        return null;
    }

}
