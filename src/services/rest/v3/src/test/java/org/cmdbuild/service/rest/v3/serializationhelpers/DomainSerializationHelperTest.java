/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package org.cmdbuild.service.rest.v3.serializationhelpers;

import static java.util.Arrays.asList;
import java.util.Map;
import org.cmdbuild.dao.beans.DomainMetadataImpl;
import org.cmdbuild.dao.entrytype.AttributeWithoutOwner;
import org.cmdbuild.dao.entrytype.AttributeWithoutOwnerImpl;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.ClasseImpl;
import org.cmdbuild.dao.entrytype.Domain;
import org.cmdbuild.dao.entrytype.DomainImpl;
import org.cmdbuild.dao.entrytype.attributetype.LookupAttributeType;
import org.cmdbuild.service.rest.common.serializationhelpers.JsonEcqlFilterHelper;
import org.cmdbuild.translation.ObjectTranslationService;
import static org.cmdbuild.utils.lang.CmMapUtils.map;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author afelice
 */
public class DomainSerializationHelperTest {

    public static final String IN_CLASS_REFERENCE_FILTERS_JSON_ATTR = "inClassReferenceFilters";

    public static final String OUT_CLASS_REFERENCE_FILTERS_JSON_ATTR = "_outClassReferenceFilters";
    public static final String SOURCE_FILTER_JSON_ATTR = JsonEcqlFilterHelper.SOURCE_CLASS_LABEL;
    public static final String DESTINATION_FILTER_JSON_ATTR = JsonEcqlFilterHelper.DESTINATION_CLASS_LABEL;

    private final static String A_KNOWN_CLASS_DOMAIN_FLOOR_FILTER = "from Floor where Id in (/(select \"Id\" from \"Floor\" where \"Building\" = 0{client:Building.Id})/)";
    private final static String A_KNOWN_CLASS_DOMAIN_FLOOR_FILTER_ECQL_ID = "7sz8g6pdj4ch3drjij5ggxsq8jh659oiiufopfshppx6245ej5r3gvy3"; // EcqlId{source=DOMAIN, id=[FloorRoom, sourceFilter::Floor]}

    private final static String A_KNOWN_CLASS_DOMAIN_ROOM_FILTER = "from Room where Id in (/(select \"Id\" from \"Room\" where \"Building\" = 0{client:Building.Id})/)";
    private final static String A_KNOWN_CLASS_DOMAIN_ROOM_FILTER_ECQL_ID = "13igdixxyr1r5jdfojkgdmr3p1osyrszf84fzfw0lb2o9ww4g1gg8u3"; // EcqlId{source=DOMAIN, id=[FloorRoom, targetFilter::Room]}

    public static final String IN_DOMAIN_REFERENCE_FILTERS_JSON_ATTR = "inDomainReferenceFilters";
    public static final String OUT_DOMAIN_REFERENCE_FILTERS_JSON_ATTR = "_outDomainReferenceFilters";

    private final static String A_KNOWN_DOMAIN_LOOKUP_ATTRIBUTE_NAME = "MyLookupAttr";
    private final static String A_KNOWN_LOOKUP_NAME = "MyLookup";

    private final static String A_KNOWN_DOMAIN_ATTRIBUTE_FILTER = "from LookUp where Id in (/(select \"Id\" from \"LookUp\" where \"Code\" in {0{client:wantedValues}})/)";
    private final static String A_KNOWN_DOMAIN_ATTRIBUTE_FILTER_ECQL_ID = "bqp0gdu06fhjw3tyjxyndgqr6h7z4efodwclp69ynqmrf"; // EcqlId{source=DOMAIN_ATTRIBUTE, id=[FloorRoom,MyLookupAttr]}

    private final ObjectTranslationService translationService = mock(ObjectTranslationService.class);

    private static DomainSerializationHelper instance;

    @Before
    public void init() {
        UniqueTestIdUtils.prepareTuid();

        instance = new DomainSerializationHelper(translationService);
    }

    /**
     * Test of serializeDetailedDomain method, for source class domain filter, of class DomainSerializationHelper.
     */
    @Test
    public void testSerializeDetailedDomain_SourceClassDomainFilter() {
        System.out.println("serializeDetailedDomain_SourceClassDomainFilter");

        //arrange:
        Map<String, String> aClassReferenceFilters = map(
                Domain.DOMAIN_SOURCE_CLASS_TOKEN, A_KNOWN_CLASS_DOMAIN_FLOOR_FILTER // contains raw cql
        );
        Classe floor = mockBuildClass("Floor");
        Classe room = mockBuildClass("Room");
        Domain domain = new DomainImpl.DomainImplBuilder()
                                        .withName("FloorRoom")
                                        .withId(1L)
                                        .withClass1(floor) // source
                                        .withClass2(room) // target
                                        .withMetadata(DomainMetadataImpl.builder()
                        .withInClassReferenceFilters(aClassReferenceFilters)                                                                .build())
                                        .build();

        //act:
        Map<String, Object> result = instance.serializeDetailedDomain(domain);

        //assert:
        assertTrue(result.containsKey(IN_CLASS_REFERENCE_FILTERS_JSON_ATTR));
        Map<String, Object> resultInClassReferenceFilters = (Map<String, Object>) result.get(IN_CLASS_REFERENCE_FILTERS_JSON_ATTR);
        assertTrue(resultInClassReferenceFilters.containsKey(SOURCE_FILTER_JSON_ATTR));
        assertEquals(A_KNOWN_CLASS_DOMAIN_FLOOR_FILTER, resultInClassReferenceFilters.get(SOURCE_FILTER_JSON_ATTR));
        assertTrue(result.containsKey(OUT_CLASS_REFERENCE_FILTERS_JSON_ATTR));
        Map<String, Object> resultOutClassReferenceFilters = (Map<String, Object>) result.get(OUT_CLASS_REFERENCE_FILTERS_JSON_ATTR);
        assertTrue(resultOutClassReferenceFilters.containsKey(SOURCE_FILTER_JSON_ATTR));
        assertEquals(A_KNOWN_CLASS_DOMAIN_FLOOR_FILTER, resultOutClassReferenceFilters.get(SOURCE_FILTER_JSON_ATTR));
        assertTrue(resultOutClassReferenceFilters.containsKey(SOURCE_FILTER_JSON_ATTR + "_ecqlFilter"));
        assertThat(resultOutClassReferenceFilters.get(SOURCE_FILTER_JSON_ATTR + "_ecqlFilter"), instanceOf(Map.class));

        Map<String, Object> resultEcqlFilter = (Map<String, Object>) resultOutClassReferenceFilters.get(SOURCE_FILTER_JSON_ATTR + "_ecqlFilter");
        assertEquals(A_KNOWN_CLASS_DOMAIN_FLOOR_FILTER_ECQL_ID, resultEcqlFilter.get("id"));
        Map<String, Object> resultEcqlFilterBindings = (Map<String, Object>) resultEcqlFilter.get("bindings");
        assertEquals(asList("Building.Id"), resultEcqlFilterBindings.get("client"));
    }

    /**
     * Test of serializeDetailedDomain method, for destination class domain filter, of class DomainSerializationHelper.
     */
    @Test
    public void testSerializeDetailedDomain_DestinationClassDomainFilter() {
        System.out.println("serializeDetailedDomain_DestinationClassDomainFilter");

        //arrange:
        Map<String, String> aClassReferenceFilters = map(
                Domain.DOMAIN_TARGET_CLASS_TOKEN, A_KNOWN_CLASS_DOMAIN_ROOM_FILTER // contains raw cql
        );
        Classe floor = mockBuildClass("Floor");
        Classe room = mockBuildClass("Room");
        Domain domain = new DomainImpl.DomainImplBuilder()
                                        .withName("FloorRoom")
                                        .withId(1L)
                                        .withClass1(floor) // source
                                        .withClass2(room) // target
                                        .withMetadata(DomainMetadataImpl.builder()
                        .withInClassReferenceFilters(aClassReferenceFilters)                                                                .build())
                                        .build();

        //act:
        Map<String, Object> result = instance.serializeDetailedDomain(domain);

        //assert:
        assertTrue(result.containsKey(IN_CLASS_REFERENCE_FILTERS_JSON_ATTR));
        Map<String, Object> resultInClassReferenceFilters = (Map<String, Object>) result.get(IN_CLASS_REFERENCE_FILTERS_JSON_ATTR);
        assertTrue(resultInClassReferenceFilters.containsKey(DESTINATION_FILTER_JSON_ATTR));
        assertTrue(result.containsKey(OUT_CLASS_REFERENCE_FILTERS_JSON_ATTR));
        Map<String, Object> resultOutClassReferenceFilters = (Map<String, Object>) result.get(OUT_CLASS_REFERENCE_FILTERS_JSON_ATTR);
        assertTrue(resultOutClassReferenceFilters.containsKey(DESTINATION_FILTER_JSON_ATTR));
        assertEquals(A_KNOWN_CLASS_DOMAIN_ROOM_FILTER, resultOutClassReferenceFilters.get(DESTINATION_FILTER_JSON_ATTR));
        assertTrue(resultOutClassReferenceFilters.containsKey(DESTINATION_FILTER_JSON_ATTR + "_ecqlFilter"));
        assertThat(resultOutClassReferenceFilters.get(DESTINATION_FILTER_JSON_ATTR + "_ecqlFilter"), instanceOf(Map.class));

        Map<String, Object> resultEcqlFilter = (Map<String, Object>) resultOutClassReferenceFilters.get(DESTINATION_FILTER_JSON_ATTR + "_ecqlFilter");
        assertEquals(A_KNOWN_CLASS_DOMAIN_ROOM_FILTER_ECQL_ID, resultEcqlFilter.get("id"));
        Map<String, Object> resultEcqlFilterBindings = (Map<String, Object>) resultEcqlFilter.get("bindings");
        assertEquals(asList("Building.Id"), resultEcqlFilterBindings.get("client"));
    }

    /**
     * Test of serializeDetailedDomain method, for both (source and target) class domain filter, of class DomainSerializationHelper.
     */
    @Test
    public void testSerializeDetailedDomain_BothClassDomainFilter() {
        System.out.println("serializeDetailedDomain_BothClassDomainFilter");

        //arrange:
        Map<String, String> aClassReferenceFilters = map(
                Domain.DOMAIN_SOURCE_CLASS_TOKEN, A_KNOWN_CLASS_DOMAIN_FLOOR_FILTER, // contains raw cql,
                Domain.DOMAIN_TARGET_CLASS_TOKEN, A_KNOWN_CLASS_DOMAIN_FLOOR_FILTER // contains raw cql
        );
        Classe floor = mockBuildClass("Floor");
        Classe room = mockBuildClass("Room");
        Domain domain = new DomainImpl.DomainImplBuilder()
                                        .withName("FloorRoom")
                                        .withId(1L)
                                        .withClass1(floor) // source
                                        .withClass2(room) // target
                                        .withMetadata(DomainMetadataImpl.builder()
                        .withInClassReferenceFilters(aClassReferenceFilters)
                        .build()
                )                                        .build();

        //act:
        Map<String, Object> result = instance.serializeDetailedDomain(domain);

        //assert:
        Map<String, Object> resultInClassReferenceFilters = (Map<String, Object>) result.get(IN_CLASS_REFERENCE_FILTERS_JSON_ATTR);
        assertTrue(resultInClassReferenceFilters.containsKey(SOURCE_FILTER_JSON_ATTR));
        assertEquals(A_KNOWN_CLASS_DOMAIN_FLOOR_FILTER, resultInClassReferenceFilters.get(SOURCE_FILTER_JSON_ATTR));
        assertTrue(resultInClassReferenceFilters.containsKey(DESTINATION_FILTER_JSON_ATTR));
        assertEquals(A_KNOWN_CLASS_DOMAIN_FLOOR_FILTER, resultInClassReferenceFilters.get(DESTINATION_FILTER_JSON_ATTR));
        Map<String, Object> resultOutClassReferenceFilters = (Map<String, Object>) result.get(OUT_CLASS_REFERENCE_FILTERS_JSON_ATTR);
        assertTrue(resultOutClassReferenceFilters.containsKey(SOURCE_FILTER_JSON_ATTR));
        assertTrue(resultOutClassReferenceFilters.containsKey(DESTINATION_FILTER_JSON_ATTR));
    }

    /**
     * Test of serializeDetailedDomain method, for domain attributes filter, of
     * class DomainSerializationHelper.
     */
    @Test
    public void testSerializeDetailedDomain_DomainAttributesFilter() {
        System.out.println("serializeDetailedDomain_DomainAttributesFilter");

        //arrange:
        Map<String, String> aReferenceFilters = map(
                A_KNOWN_DOMAIN_LOOKUP_ATTRIBUTE_NAME, A_KNOWN_DOMAIN_ATTRIBUTE_FILTER // contains raw cql
        );
        Classe floor = mockBuildClass("Floor");
        Classe room = mockBuildClass("Room");
        Domain domain = new DomainImpl.DomainImplBuilder()
                .withName("FloorRoom")
                .withId(1L)
                .withClass1(floor) // source
                .withClass2(room) // target
                .withAttribute(buildDomainAttribute(A_KNOWN_DOMAIN_LOOKUP_ATTRIBUTE_NAME, A_KNOWN_LOOKUP_NAME))
                .withMetadata(DomainMetadataImpl.builder()
                        .withInDomainReferenceFilters(aReferenceFilters)
                        .build())
                .build();

        //act:
        Map<String, Object> result = instance.serializeDetailedDomain(domain);

        //assert:
        assertTrue(result.containsKey(IN_DOMAIN_REFERENCE_FILTERS_JSON_ATTR));
        Map<String, Object> resultInDomainReferenceFilters = (Map<String, Object>) result.get(IN_DOMAIN_REFERENCE_FILTERS_JSON_ATTR);
        assertTrue(resultInDomainReferenceFilters.containsKey(A_KNOWN_DOMAIN_LOOKUP_ATTRIBUTE_NAME));
        assertEquals(A_KNOWN_DOMAIN_ATTRIBUTE_FILTER, resultInDomainReferenceFilters.get(A_KNOWN_DOMAIN_LOOKUP_ATTRIBUTE_NAME));
        assertTrue(result.containsKey(OUT_DOMAIN_REFERENCE_FILTERS_JSON_ATTR));
        Map<String, Object> resultOutDomainReferenceFilters = (Map<String, Object>) result.get(OUT_DOMAIN_REFERENCE_FILTERS_JSON_ATTR);
        assertTrue(resultOutDomainReferenceFilters.containsKey(A_KNOWN_DOMAIN_LOOKUP_ATTRIBUTE_NAME));
        assertTrue(resultOutDomainReferenceFilters.containsKey(A_KNOWN_DOMAIN_LOOKUP_ATTRIBUTE_NAME + "_ecqlFilter"));
        assertThat(resultOutDomainReferenceFilters.get(A_KNOWN_DOMAIN_LOOKUP_ATTRIBUTE_NAME + "_ecqlFilter"), instanceOf(Map.class));

        Map<String, Object> resultEcqlFilter = (Map<String, Object>) resultOutDomainReferenceFilters.get(A_KNOWN_DOMAIN_LOOKUP_ATTRIBUTE_NAME + "_ecqlFilter");
        assertEquals(A_KNOWN_DOMAIN_ATTRIBUTE_FILTER_ECQL_ID, resultEcqlFilter.get("id"));
        Map<String, Object> resultEcqlFilterBindings = (Map<String, Object>) resultEcqlFilter.get("bindings");
        assertEquals(asList("wantedValues"), resultEcqlFilterBindings.get("client"));
    }

    private static ClasseImpl mockBuildClass(String classeName) {
        return ClasseImpl.builder()
                .withName(classeName)
                .build();
    }

    private AttributeWithoutOwner buildDomainAttribute(String attribName, String lookupName) {
        return AttributeWithoutOwnerImpl.builder().withName(attribName).withType(new LookupAttributeType(lookupName)).build();
    }

} // end DomainSerializationHelperTest class

/**
 * Duplicated here from module cmdbuild-test-framework to not import all that module only to use this class.
 *
 * @author afelice
 */
class UniqueTestIdUtils {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private static int i = 0;

    public static void prepareTuid() {
        i++;
    }

    /**
     * @return test unique id (to prefix names and stuff)
     */
    public static String tuid() {
        return Integer.toString(i, 32);
    }

    /**
     * @param id
     * @return param + test unique id
     */
    public static String tuid(String id) {
        return id + tuid();//StringUtils.capitalizeFirstLetter(tuid());
    }

} // end UniqueTestIdUtils class