/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cmdbuild.service.rest.v3.serializationhelpers;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Map;
import static java.util.stream.Collectors.toList;
import org.apache.commons.lang3.StringUtils;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.cmdbuild.dao.beans.RelationDirection.RD_DIRECT;
import static org.cmdbuild.dao.beans.RelationDirection.RD_INVERSE;
import static org.cmdbuild.dao.entrytype.ClassPermission.CP_UPDATE;
import org.cmdbuild.dao.entrytype.Classe;
import org.cmdbuild.dao.entrytype.Domain;
import static org.cmdbuild.dao.entrytype.Domain.DOMAIN_SOURCE_CLASS_TOKEN;
import static org.cmdbuild.dao.entrytype.Domain.DOMAIN_TARGET_CLASS_TOKEN;
import static org.cmdbuild.dao.utils.DomainUtils.getActualCascadeAction;
import static org.cmdbuild.dao.utils.DomainUtils.serializeDomainCardinality;
import org.cmdbuild.ecql.EcqlBindingInfo;
import static org.cmdbuild.ecql.utils.EcqlUtils.buildAttrEcqlId;
import static org.cmdbuild.ecql.utils.EcqlUtils.buildDomainEcqlId;
import static org.cmdbuild.ecql.utils.EcqlUtils.buildDomainMasterDetailEcqlId;
import static org.cmdbuild.ecql.utils.EcqlUtils.buildUniqueClassToken;
import static org.cmdbuild.ecql.utils.EcqlUtils.getEcqlBindingInfoForExpr;
import static org.cmdbuild.ecql.utils.EcqlUtils.getEcqlExpression;
import static org.cmdbuild.service.rest.common.serializationhelpers.AttributeTypeConversionService.OUT_CLASS_REFERENCE_FILTERS_TOKEN;
import static org.cmdbuild.service.rest.common.serializationhelpers.AttributeTypeConversionService.OUT_DOMAIN_REFERENCE_FILTERS_TOKEN;
import static org.cmdbuild.service.rest.common.serializationhelpers.EcqlFilterSerializationHelper.addEcqlFilter;
import static org.cmdbuild.service.rest.common.serializationhelpers.EcqlFilterSerializationHelper.buildEcqlFilterStuff;
import org.cmdbuild.service.rest.common.serializationhelpers.JsonEcqlFilterHelper;
import org.cmdbuild.translation.ObjectTranslationService;
import org.cmdbuild.utils.lang.CmCollectionUtils;
import static org.cmdbuild.utils.lang.CmConvertUtils.serializeEnum;
import org.cmdbuild.utils.lang.CmMapUtils.FluentMap;
import static org.cmdbuild.utils.lang.CmMapUtils.map;
import static org.cmdbuild.utils.lang.CmMapUtils.toMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DomainSerializationHelper {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public static final String IN_DOMAIN_REFERENCE_FILTERS_TOKEN = "inDomainReferenceFilters";
    public static final String IN_CLASS_REFERENCE_FILTERS_TOKEN = "inClassReferenceFilters";

    private final ObjectTranslationService translationService;

    public DomainSerializationHelper(ObjectTranslationService translationService) {
        this.translationService = checkNotNull(translationService);
    }

    public FluentMap<String, Object> serializeBasicDomain(Domain domain) {
        return map("_id", domain.getName(),
                "name", domain.getName(),
                "description", domain.getDescription());
    }

    public FluentMap<String, Object> serializeDetailedDomain(Domain domain) {
        return serializeBasicDomain(domain).with("source", domain.getSourceClass().getName(),
                "sources", domain.getSourceClasses().stream().map(Classe::getName).sorted().collect(toList()),
                "sourceProcess", domain.getSourceClass().isProcess(),
                "destination", domain.getTargetClass().getName(),
                "destinations", domain.getTargetClasses().stream().map(Classe::getName).sorted().collect(toList()),
                "destinationProcess", domain.getTargetClass().isProcess(),
                "cardinality", serializeDomainCardinality(domain.getCardinality()),
                "descriptionDirect", domain.getDirectDescription(),
                "_descriptionDirect_translation", translationService.translateDomainDirectDescription(domain.getName(), domain.getDirectDescription()),
                "descriptionInverse", domain.getInverseDescription(),
                "_descriptionInverse_translation", translationService.translateDomainInverseDescription(domain.getName(), domain.getInverseDescription()),
                "indexDirect", domain.getIndexForSource(),
                "indexInverse", domain.getIndexForTarget(),
                "descriptionMasterDetail", domain.getMasterDetailDescription(),
                "_descriptionMasterDetail_translation", translationService.translateDomainMasterDetailDescription(domain.getName(), domain.getMasterDetailDescription()),
                "filterMasterDetail", domain.getMasterDetailFilter(),
                "isMasterDetail", domain.isMasterDetail(),
                "sourceInline", domain.getMetadata().isSourceInline(),
                "sourceDefaultClosed", domain.getMetadata().isSourceDefaultClosed(),
                "destinationInline", domain.getMetadata().isTargetInline(),
                "destinationDefaultClosed", domain.getMetadata().isTargetDefaultClosed(),
                "active", domain.isActive(),
                "disabledSourceDescendants", CmCollectionUtils.toList(domain.getDisabledSourceDescendants()),
                "disabledDestinationDescendants", CmCollectionUtils.toList(domain.getDisabledTargetDescendants()),
                "masterDetailAggregateAttrs", CmCollectionUtils.toList(domain.getMasterDetailAggregateAttrs()),
                "masterDetailDisabledCreateAttrs", CmCollectionUtils.toList(domain.getMasterDetailDisabledCreateAttrs()),
                "cascadeActionDirect", serializeEnum(domain.getMetadata().getCascadeActionDirect()),
                "cascadeActionInverse", serializeEnum(domain.getMetadata().getCascadeActionInverse()),
                "_cascadeActionDirect_actual", serializeEnum(getActualCascadeAction(domain, domain.getMetadata().getCascadeActionDirect(), RD_DIRECT)),
                "_cascadeActionInverse_actual", serializeEnum(getActualCascadeAction(domain, domain.getMetadata().getCascadeActionInverse(), RD_INVERSE)),
                "cascadeActionDirect_askConfirm", domain.getMetadata().getCascadeActionDirectAskConfirm(),
                "cascadeActionInverse_askConfirm", domain.getMetadata().getCascadeActionInverseAskConfirm(),
                "_can_create", domain.hasServicePermission(CP_UPDATE))
                .accept(m -> ecqlClassReferenceFilterStuff(m, DOMAIN_SOURCE_CLASS_TOKEN, domain.getSourceClass(), domain.getSourceClassReferenceFilter(), domain))
                .accept(m -> ecqlClassReferenceFilterStuff(m, DOMAIN_TARGET_CLASS_TOKEN, domain.getTargetClass(), domain.getTargetClassReferenceFilter(), domain))
                .accept(m -> ecqlDomainAttributeFilterStuff(m, domain.getDomainAttributeReferenceFilters(), domain))
                .accept(m -> ecqlClassMasterDetailFilterStuff(m, domain));
    }

    /**
     * Update <code>classReferenceFilters</code> attribute with given
     * source/destination class filter.
     *
     * @param domainData
     * @param classeToken <code>sourceFilter</code> or <code>targetFilter</code>
     * @param classe
     * @param filter the cql filter
     * @param domain
     * @param metadata
     */
    private void ecqlClassReferenceFilterStuff(FluentMap<String, Object> domainData, String classeToken, Classe classe, String filter, Domain domain) {
        if (isEmpty(filter)) {
            return;
        }
        // Map to internal model key to use (may be different from UI used label)
        String key = JsonEcqlFilterHelper.fromModel(classeToken);
        // Add input filter
        appendToMap(domainData, IN_CLASS_REFERENCE_FILTERS_TOKEN, key, filter);
        // Add output EcqlId
        EcqlBindingInfo ecqlBindingInfo = getEcqlBindingInfoForExpr(getEcqlExpression(domain.getMetadata(), filter));
        final String ecqlId = buildDomainEcqlId(domain, buildUniqueClassToken(classeToken, classe));

        addToReferenceFilters(domainData, key, OUT_CLASS_REFERENCE_FILTERS_TOKEN, filter, ecqlId, ecqlBindingInfo);
    }

    /**
     *
     * @param domainData
     * @param key one of
     * <ul>
     * <li><code>sourceFilter</code>;
     * <li><code>destinationFilter</code>;
     * <li><code>&lt;domain_attribute_name&gt;</code>.
     * </ul>
     * outReferenceFiltersToken one of
     * <ul>
     * <li><code>outClassReferenceFilters</code>;
     * <li><code>outDomainReferenceFilters</code>.
     * </ul>
     * @param filter the cql filter
     * @param ecqlId the EcqlId
     * @param ecqlBindingInfo
     */
    private void addToReferenceFilters(FluentMap<String, Object> domainData, String key, final String outReferenceFiltersToken, String filter, final String ecqlId, EcqlBindingInfo ecqlBindingInfo) {
        domainData.with(outReferenceFiltersToken, fetchMap(domainData, outReferenceFiltersToken).with(buildEcqlFilterStuff(
                key, filter,
                key + "_ecqlFilter", ecqlId,
                ecqlBindingInfo))
        );
    }

    /**
     * Update <code>classReferenceFilters</code> attribute with given domain
     * attribute filter.
     *
     * @param domainData
     * @param attributeFilters the cql filters, with key the domain's attribute
     * name to apply to.
     * @param domain
     * @param metadata contains domain attributes filter in a map with key the
     * attribute name.
     */
    private void ecqlDomainAttributeFilterStuff(FluentMap<String, Object> domainData, Map<String, String> attributeFilters, Domain domain) {
        logger.debug("attaching filter serialization for attributes {}", attributeFilters.keySet());

        map(attributeFilters).withoutValues(StringUtils::isEmpty).forEach((attributeName, filter) -> {
            // Add input filter
            appendToMap(domainData, IN_DOMAIN_REFERENCE_FILTERS_TOKEN, attributeName, filter);

            EcqlBindingInfo ecqlBindingInfo = getEcqlBindingInfoForExpr(getEcqlExpression(domain.getMetadata(), filter));
            final String ecqlId = buildAttrEcqlId(domain.getAttribute(attributeName));
            addToReferenceFilters(domainData, attributeName, OUT_DOMAIN_REFERENCE_FILTERS_TOKEN, filter, ecqlId, ecqlBindingInfo);
        });
    }

    private FluentMap<String, Object> fetchMap(FluentMap<String, Object> domainData, String key) {
        if (!domainData.containsKey(key)) {
            return map();
        }
        return toMap(domainData.get(key));
    }

    private void appendToMap(FluentMap<String, Object> domainData, String attribName, String key, String filter) {
        domainData.put(attribName, fetchMap(domainData, attribName).with(key, filter));
    }

    private void ecqlClassMasterDetailFilterStuff(FluentMap<String, Object> domainData, Domain domain) {
        String filter = domain.getMasterDetailFilter();
        if (!domain.isMasterDetail() || isEmpty(filter)) {
            return;
        }

        EcqlBindingInfo ecqlBindingInfo = getEcqlBindingInfoForExpr(getEcqlExpression(domain.getMetadata(), filter));
        final String ecqlId = buildDomainMasterDetailEcqlId(domain);
        addEcqlFilter(domainData, "ecqlFilterMasterDetail", ecqlId, ecqlBindingInfo);
    }

}
