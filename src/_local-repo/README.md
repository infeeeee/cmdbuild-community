# Local repo

This directory contains prebuilt maven requirements not available on other sources any more.

## Usage

Add this to `pom.xml`:

```xml
    <repositories>
        [...]
        <repository>
            <id>local-repo</id>
            <url>file://_local-repo</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
    </repositories>
```

## Sources

### jxpdl-1.3-1

- https://sourceforge.net/projects/jxpdl/
- https://github.com/si294r/sourceforge-jxpdl

### cmdbuild-utils-modbus-3.4.2

Extracted from cmdbuild prebuilt CMDbuilt.war
