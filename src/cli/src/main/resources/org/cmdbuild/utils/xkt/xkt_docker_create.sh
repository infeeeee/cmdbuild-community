#!/bin/bash

dockerimagename="cmdbuild-xeokit:#cmdbuildversion#"
dockerfile="$1"

echo "Creating $dockerimagename docker image"

docker build -t $dockerimagename $dockerfile

echo "docker image created"
