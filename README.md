# CMDBuild-community

Community fixes, documentation and builds for CMDBuild. 

Open an issue if something is not clear!

⚠️ **Do not ask about this version on the CMDBuild forum!** ⚠️

⚠️ **This fork is not supported by Tecnoteca srl.** ⚠️


## Run

### Docker compose

Docker compose files in the `compose` directory. Modify timezone, and other envvars as you wish.


```shell
docker compose -f compose/docker-compose-cmdbuild.yaml up -d
docker compose -f compose/docker-compose-ready2use.yaml up -d
docker compose -f compose/docker-compose-openmaint.yaml up -d

# Openmaint All In One: openmaint + bimserver + opencms:
docker compose -f compose/docker-compose-openmaint-aio.yaml up -d
```

### War

Wars can be downloaded from Gitlab Package Registry:

https://gitlab.com/infeeeee/cmdbuild-community/-/packages

Follow official install docs

## Tips

### Reverse proxy

https://forum.cmdbuild.org/t/cmdbuild-behind-nginx-reverse-proxy/5070/4

### OpenCMS

Login: http://localhost:8092/system/login/

To use without category management, set this in openmaint Administration UI:
- Classes -> Standard -> Site -> Building -> Edit -> DMS category -> Default
- DMS -> Settings:
  - Service type: `CMIS`
  - Host: `http://openmaint-opencms:8080/cmisatom`
  - Webservice path: (Leave empty)
  - Username: `Admin`
  - Password: set up with `$ADMIN_PASSWD` envvar, defaults to `admin`

### BIMserver

On Administration UI -> BIM -> Settings:
- URL: `http://openmaint-bimserver:8080`
- Username, Password 
 
Some notes about importing IFC files in the [OSArch wiki](https://wiki.osarch.org/index.php?title=OpenMAINT)

### Backup and restore

#### Backup

To backup you only have to backup the db. Replace `openmaint-db` with the name of the db container to dump:

```shell
docker exec -t openmaint-db pg_dumpall -c -U postgres > openmaint_dump.sql
```

Bimserver: save the contents of `/usr/local/bimserver/home` from the container


#### Restore

Stop the application containers than run this command to restore:

```shell
cat openmaint_dump.sql | docker exec -i openmaint-db psql -U postgres postgres
```


## Build

### Build in docker

Prerequisites: docker, git

```shell
git clone https://gitlab.com/infeeeee/cmdbuild-community
cd cmdbuild-community

# Build all docker images:
docker compose -f docker/docker-compose-build.yaml build

# Build only one image:
docker compose -f docker/docker-compose-build.yaml build cmdbuild
docker compose -f docker/docker-compose-build.yaml build openmaint
docker compose -f docker/docker-compose-build.yaml build ready2use

# Build wars:
docker compose -f docker/docker-compose-build-war.yaml run --rm --build cmdbuild
docker compose -f docker/docker-compose-build-war.yaml run --rm --build openmaint
docker compose -f docker/docker-compose-build-war.yaml run --rm --build ready2use
```

Wars will be saved to the `build` directory.

## Comparison to upstream

### Missing source code

The following modules are missing from the upstream sources of cmdbuild:

| Module name                | Solution                 |
| -------------------------- | ------------------------ |
| `utils/bugreportcollector` | Not built, not necessary |
| `utils/modbus`             | Downloaded from release  |

Other missing files:

| File name         | Description                            | Solution               |
| ----------------- | -------------------------------------- | ---------------------- |
| `.mvn/jvm.config` | Needed for cli package (`cmdbuild.sh`) | No cli build, only war |


### Fixes in this repo

#### 3.4.2

Related to running in Docker:

- `src/ui/pom.xml`: UI building and setup in separate container
- `src/cmdbuild/src/main/webapp/cmdbuild.sh`: No checking for correct java version
- `src/core/system/src/main/java/org/cmdbuild/minions/MinionServiceImpl.java`: Allow running as root

#### 3.4.2-1

- Bimserver client update to `1.5.182` - working now!

#### 3.4.2-2

- IfcPlugins updated to `0.0.98` - ifc import working!


## Development

### Version Matrix

| CMDBuild | Vertical | Java | Tomcat | Postgres | Postgis | BIMServer |
| -------- | -------- | ---- | ------ | -------- | ------- | --------- |
| 3.4.2    | 2.3      | 17   | 9.0.75 | 12       | 3.3     | 1.5.138   |
| 3.4.2-1  | 2.3      | 17   | 9.0.75 | 12       | 3.3     | 1.5.182   |
| 3.4.2-2  | 2.3      | 17   | 9.0.75 | 12       | 3.3     | 1.5.182   |
| 3.4.3    | 2.3      | 17   | 9.0.82 | 12-15    | 3.4     | 1.5.184   |


### Version numbers

- `openmaint-x.x-y.y.y-z`
- `ready2use-x.x-y.y.y-z`
- `cmdbuild-y.y.y-z`

Meaning:

- `x.x`: vertical version as upstream
- `y.y.y`: core version as upstream
- `z`: updates in this repo

### Branches

- `main`: community modifications
- `upstream`: source from SourceForge as-is. 
 
### Update `upstream` branch

Empty the existing dirs before copying there, not just overwrite, so file deletions can be also tracked!

1. Download from [CMDBuild SourceForge](https://sourceforge.net/projects/cmdbuild/files/):

- Download `cmdbuild-y.y.y-src.tar.gz`, extract it to `src` dir
- Download `cmdbuild-y.y.y-resources.tar.gz`,  extract it to `resources` dir
- Download `README.txt`, `CHANGELOG.txt`, `COPYING.txt`, extract it to project root
- Download `ready2use-x.x-y.y.y-resources.tar.gz`  extract contents of `database` dir to `resources/database-ready2use` dir

2. Download from [openMAINT SourceForge](https://sourceforge.net/projects/openmaint/files):

- Download `openmaint-x.x-y.y.y-resources.tar.gz`, extract contents of `database` dir to `resources/database-openmaint` dir

3. Git:

```shell
git add .
git commit --message="y.y.y" --date="2023-07-05 09:39"
```

### Directories

- `build`: build wars
- `compose`: compose files for running the containers
- `docker`: files for building in docker
- `resources`: resources for running
- `src`: source code

## Motivation

CMDBuild is a genial software, the opensource community needs solutions like this! We are very thankful for the developer for creating such a beautiful tool, and releasing it with a modern, suitable copyleft license. Thank you Tecnoteca!

However there are some issues about how they handle the project:

### Nonexistent support

Maintainers of CMDBuild rarely reply to any questions and requests on the official forum, just random users shouting to the empty void. E.g. I found a bug in the software, I could reproduce on multiple platforms no response, nothing.

### Lacking documentation

No detailed build instructions, documentation exist. 

### No modern install methods

Only official install methods are via GUI and terminal. No Docker or similar container instructions, no VM images. Docker images bilt from prebuilt require some quirky `chmod` workaround.


## License and legal stuff

Everything is GNU-AGPL-3 as upstream. 

Specific license info of the vertical solutions on their websites:

- CMDBuild: https://www.cmdbuild.org/en/project/license
- CMDBuild READY2USE: https://www.cmdbuildready2use.org/en/download/license
- openMAINT: https://www.openmaint.org/en/download/license

Following from CMDBuild source:

### LICENSE

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License 
along with this program.
If not, see <http://www.gnu.org/licenses/agpl.html>.



### COPYRIGHT AND OWNERSHIP

Title, copyrights, intellectual property rights, international treaty, 
other rights as applicable and all other legal rights in the Software 
and Documentation are and shall remain the sole and exclusive property 
of Tecnoteca srl or its suppliers. 
As well all modifications, enhancements, derivatives and other 
alterations of the Software and Documentation regardless of who made 
any modifications, if any, are and shall remain the sole and exclusive 
property of Tecnoteca srl or its suppliers. 
The license for use granted herein is limited to the Software and 
Documentation and does not transfer any ownership rights from Tecnoteca 
to End Users or any other intellectual property rights.  


### LEGAL NOTICES

The interactive user interfaces in modified source and object code 
versions of this program must display Appropriate Legal Notices, as 
required under Section 5 of the GNU General Public License version 3.



### TRADEMARKS

CMDBuild, CMDBuild READY2USE, openMAINT trademarks can't be altered 
(colors, font, shape, etc) and can't be included in other trademarks.
CMDBuild, CMDBuild READY2USE, openMAINT trademarks can't be used as 
a company logo, moreover no company can act as CMDBuild or
CMDBuild READY2USE or openMAINT author/owner/maintainer.
CMDBuild, CMDBuild READY2USE, openMAINT trademarks can't be removed 
from the parts of the application in which are reported, and in 
particular from the header at the top of each page.

In accordance with Section 7(b) of the GNU General Public License 
version 3, these Appropriate Legal Notices must retain the display 
of the "CMDBuild", "CMDBuild READY2USE" or "openMAINT" logo.

The Logo "CMDBuild" must be a clickable link that leads directly 
to the Internet URL https://www.cmdbuild.org

The Logo "CMDBuild READY2USE" must be a clickable link that leads directly 
to the Internet URL https://www.cmdbuildready2use.org

The Logo "openMAINT" must be a clickable link that leads directly 
to the Internet URL https://www.openmaint.org
