#!/bin/bash

WAR_NAME="cmdbuild-${CMDBUILD_VERSION}"

if [[ -n "$VERTICAL_NAME" ]]; then
    WAR_NAME="${VERTICAL_NAME}-${VERTICAL_VERSION}-${CMDBUILD_VERSION}"
fi

cp "${CMDBUILD_SOURCE_DIR}/cmdbuild/target/cmdbuild.war" "${CMDBUILD_BUILD_DIR}/${WAR_NAME}.war"
