#!/bin/bash

# Create DB config from envvars:
mkdir -p "$CATALINA_HOME/conf/cmdbuild"
cat >"$CATALINA_HOME/conf/cmdbuild/database.conf" <<EOL
# Do not modify this file, it will be overwritten on next restart!
# Use Environment Variables!
db.url=jdbc:postgresql://${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}
db.username=cmdbuild
db.password=cmdbuild
db.admin.username=${POSTGRES_USER}
db.admin.password=${POSTGRES_PASS}
EOL

#Change tomcat admin password. If nothing set, generate a random password
if [[ -z "$TOMCAT_PASSWORD" ]]; then
    TPASSWORD=$(echo $RANDOM | md5sum | cut -c 1-10)
else
    TPASSWORD=${TOMCAT_PASSWORD}
fi
OLDSTRING="username=\"admin\" password=\".*\" roles=\"manager-gui\""
NEWSTRING="username=\"admin\" password=\"${TPASSWORD}\" roles=\"manager-gui\""

sed -i -- "s/${OLDSTRING}/${NEWSTRING}/g" "${CATALINA_HOME}/conf/tomcat-users.xml"


# first init DB, second start with fail
while ! timeout 1 bash -c "echo > /dev/tcp/${POSTGRES_HOST}/${POSTGRES_PORT}"; do
    echo >&2 "Postgres is unavailable - sleeping"
    sleep 5
done

echo "Init DB"
{ # try
    chmod +x "${CATALINA_HOME}/webapps/cmdbuild/cmdbuild.sh"
    "${CATALINA_HOME}/webapps/cmdbuild/cmdbuild.sh" dbconfig create "$CMDBUILD_DUMP" \
        -configfile "${CATALINA_HOME}/conf/cmdbuild/database.conf"

} || {
    echo "DB was initiliazed. Use dbconfig recreate or dbconfig drop"
}

#echo "RUN catalina"
exec "${CATALINA_HOME}/bin/catalina.sh" run
